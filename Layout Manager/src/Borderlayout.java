import javax.swing.*;
import javax.swing.border.LineBorder;

import java.awt.*;

public class Borderlayout extends JFrame{

	public Borderlayout(String title) {
		super(title);
		this.setSize(500, 150);
		this.setLocation(100, 100);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
		JButton button1 = new JButton("Button1");
		JButton button2 = new JButton("Button2");
		JButton button3 = new JButton("Button3");
		JButton button4 = new JButton("Button4");
		JButton button5 = new JButton("Button5");
		JButton button6 = new JButton("Button6");
		JButton button7 = new JButton("Button7");
		
		
		Container mainContainer = this.getContentPane();
		mainContainer.setLayout(new BorderLayout(8,6));
		mainContainer.setBackground(Color.WHITE);
		this.getRootPane().setBorder(BorderFactory.createMatteBorder(4,4,4,4, Color.GREEN));
		
		
		//Top Panel
		JPanel topPanel = new JPanel();
		topPanel.setBorder(new LineBorder(Color.BLACK, 3));
		topPanel.setBackground(Color.BLUE);
		topPanel.setLayout(new FlowLayout(5));
		topPanel.add(button1);
		topPanel.add(button2);
		topPanel.add(button3);
		mainContainer.add(topPanel, BorderLayout.NORTH);
		
		
		//Middle Panel
		JPanel middlePanel = new JPanel();
		middlePanel.setBorder(new LineBorder(Color.BLACK, 3));
		middlePanel.setBackground(Color.RED);
		middlePanel.setLayout(new FlowLayout(5));
		
		
		JPanel gridPanel = new JPanel();
		gridPanel.setBorder(new LineBorder(Color.BLACK, 3));
		gridPanel.setBackground(Color.RED);
		gridPanel.setLayout(new GridLayout(4, 1, 5, 5));
		middlePanel.add(gridPanel);
		gridPanel.add(button4);
		gridPanel.add(button5);
		gridPanel.add(button6);
		gridPanel.add(button7);
		
		JLabel label = new JLabel("Center Box", SwingConstants.CENTER);
		label.setOpaque(true);
		label.setBorder(new LineBorder(Color.BLACK, 3));
		
		mainContainer.add(label);
		mainContainer.add(middlePanel, BorderLayout.WEST);
		
		
		
		
		
	}
		
	
	public static void main(String[] args) {
	Borderlayout mylayout = new Borderlayout("My Layout")	;
	mylayout.setVisible(true);
	}
	
	
}
